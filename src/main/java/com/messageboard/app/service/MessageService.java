package com.messageboard.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import com.messageboard.app.domain.Message;
import com.messageboard.app.repo.MessageRepository;

@Named
public class MessageService {

	@Inject
	MessageRepository messageRepository;

	public void saveMessage(Message message) {
		messageRepository.save(message);
	}

	public Message getMessage(@NotNull UUID messageId) {
		return messageRepository.findById(messageId).get();
	}

	public List<Message> getAllUserMessages(String userName) {
		List<Message> messages = new ArrayList<>();
		messageRepository.findByUserName(userName).forEach(messages::add);
		return messages;
	}

}
