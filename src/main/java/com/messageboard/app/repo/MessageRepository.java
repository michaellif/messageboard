package com.messageboard.app.repo;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.messageboard.app.domain.Message;

public interface MessageRepository extends CrudRepository<Message, UUID> {

	List<Message> findByUserName(String userName);
}
