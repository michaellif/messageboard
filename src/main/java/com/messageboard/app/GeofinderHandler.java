package com.messageboard.app;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.messageboard.app.domain.Location;

@Component
public class GeofinderHandler {

	private String GEOCODELOCATION_URL = "https://maps.googleapis.com/maps/api/geocode/json?&address=";

	public Location findLocation(String city) {
		return findLocation(city, 0);
	}

	private Location findLocation(String city, int tryNumber) {
		Location retVal = new Location();
		if (city != null) {
			String addressEncoded;
			try {
				addressEncoded = URLEncoder.encode(city, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new Error("UnsupportedEncodingException", e);
			}
			JSONObject obj = new JSONObject(callGoogleAPI(GEOCODELOCATION_URL + addressEncoded));
			String status = obj.getString("status");

			if (status.equals("OK")) {
				System.out.println("status : " + status);
				JSONArray arr = obj.getJSONArray("results");
				if (arr.length() >= 1) {
					JSONObject placeid = arr.getJSONObject(0).getJSONObject("geometry").getJSONObject("location");
					return new Location(placeid.getDouble("lat"), placeid.getDouble("lng"));
				} else {
					throw new Error("More than one geolocation returned while calling Google map API");
				}
			} else if (status.equals("OVER_QUERY_LIMIT")) {
				if (tryNumber <= 5) {
					// OVER_QUERY_LIMIT means the times of search exceed the limit google support
					// wait one seconds , because it can only search 10 times a second.
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					retVal = findLocation(city, tryNumber++);
				}
			}
		}
		return retVal;
	}

	private String callGoogleAPI(String urlStr) {
		StringBuilder builder = new StringBuilder();
		URL url;
		try {
			url = new URL(urlStr);
			try (Scanner scan = new Scanner(url.openStream())) {
				while (scan.hasNext()) {
					builder.append(scan.nextLine());
				}
				System.out.println("Response : " + builder);
			} catch (IOException e) {
				throw new Error("Error in calling Google map API", e);
			}
		} catch (MalformedURLException e) {
			throw new Error("Malformed URL has occurred", e);
		}

		return builder.toString();
	}
}
