package com.messageboard.app.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.messageboard.app.GeofinderHandler;
import com.messageboard.app.domain.Message;
import com.messageboard.app.service.MessageService;

@Path("/")
public class MessageResource {
	
	@Inject
	private MessageService messageService;
	
	@Inject
	private GeofinderHandler geofinderHandler;
	
	@POST
	@Path("/")
	public void saveMessage(@QueryParam("value") String value, @QueryParam("userName") String userName, @QueryParam("city")  String city) {
		messageService.saveMessage(new Message(value, userName, city, geofinderHandler.findLocation(city)));
	}
	
	@GET
	@Path("/list")
	@Produces({MediaType.TEXT_HTML, MediaType.APPLICATION_JSON})
	public List<Message> getAllUserMessages(@QueryParam("userName") String userName) {
		return messageService.getAllUserMessages(userName);
	}

}
