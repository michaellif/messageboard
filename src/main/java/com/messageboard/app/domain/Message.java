package com.messageboard.app.domain;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.messageboard.app.GeofinderHandler;

@Entity
public class Message {

	@Id
	private UUID messageId;

	private Date creationDate;

	private String value;

	private String userName;

	private String city;
	
	@Embedded
	private Location location;

	public Message() {
	}

	public Message(String value, String userName, String city, Location location) {
		this.messageId = UUID.randomUUID();
		this.creationDate = new Date();
		this.value = value;
		this.userName = userName;
		this.city = city;
		this.location = location;
	}

	public Message(String value, String userName) {
		this(value, userName, null, new Location());
	}
	
	public UUID getMessageId() {
		return messageId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Location getLocation() {
		return location;
	}
	
	@Override
	public String toString() {
		return value + " (" + userName + ")";
	}
}
