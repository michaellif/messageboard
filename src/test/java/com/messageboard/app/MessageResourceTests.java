package com.messageboard.app;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.messageboard.app.domain.Message;
import com.messageboard.app.rest.MessageResource;
import com.messageboard.app.service.MessageService;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class MessageResourceTests {

	@Mock
	GeofinderHandler geofinderHandler;
	
	@Mock
	MessageService messageService;
	
	@InjectMocks
	MessageResource messageResource;

	@Test
	public void saveAndRetrieve_positive_ok() throws Exception {
		when(messageService.getAllUserMessages(anyString())).thenReturn(Arrays.asList(new Message("Hello from John", "John"), new Message("Have a nice day", "John")));
		
		assertEquals("Hello from John", messageResource.getAllUserMessages("John").get(0).getValue());
		assertEquals("Have a nice day", messageResource.getAllUserMessages("John").get(1).getValue());
	}


}
