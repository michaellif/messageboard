package com.messageboard.app;

import static org.junit.Assert.assertEquals;

import java.net.URLEncoder;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.messageboard.app.domain.Message;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MessageboardIntegrationTests {

	@Autowired
	TestRestTemplate restTemplate;
	
	@Before
	public void before() throws Exception {
		restTemplate.exchange(
				"/message?value=" + URLEncoder.encode("Hello from John", "UTF-8") + "&userName=John&city=Toronto",
				HttpMethod.POST, null, Void.class);
		restTemplate.exchange("/message?value=" + URLEncoder.encode("Have a nice day", "UTF-8") + "&userName=John&city=Toronto",
				HttpMethod.POST, null, Void.class);
		restTemplate.exchange("/message?value=" + URLEncoder.encode("Hello from Robert", "UTF-8") + "&userName=Robert&city=Ottawa",
				HttpMethod.POST, null, Void.class);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void saveAndRetrieve_positive_ok() {
		ResponseEntity<List<Message>> response1 = restTemplate.exchange("/message/list?userName=John", HttpMethod.GET,
				null, new ParameterizedTypeReference<List<Message>>() {
				});

		assertEquals("Hello from John (John)", response1.getBody().get(0).toString());
		assertEquals("Have a nice day (John)", response1.getBody().get(1).toString());

		ResponseEntity<List<Message>> response2 = restTemplate.exchange("/message/list?userName=Robert", HttpMethod.GET,
				null, new ParameterizedTypeReference<List<Message>>() {
				});

		assertEquals("Hello from Robert (Robert)", response2.getBody().get(0).toString());
	}
	
}
