package com.messageboard.app;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.messageboard.app.domain.Location;

public class GeofinderHandlerTest {

	@Test
	public void findLocation_Toronto_ok() {
		Location location = new GeofinderHandler().findLocation("Toronto");
		assertEquals(43.653226,location.getLatitude(), 0.001);
		assertEquals(-79.3831843,location.getLongitude(), 0.001);
	}
}
