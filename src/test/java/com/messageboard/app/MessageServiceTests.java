package com.messageboard.app;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.messageboard.app.domain.Message;
import com.messageboard.app.service.MessageService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MessageServiceTests {

	@Inject
	MessageService messageService;

	@Before
	public void before() throws Exception {
		messageService.saveMessage(new Message("Hello from John", "John"));
		messageService.saveMessage(new Message("Have a nice day", "John"));
		messageService.saveMessage(new Message("Hello from Robert", "Robert"));
	}
	
	@Test
	public void saveAndRetrieve_positive_ok() throws Exception {
		assertEquals("Hello from John", messageService.getAllUserMessages("John").get(0).getValue());
		assertEquals("Have a nice day", messageService.getAllUserMessages("John").get(1).getValue());
		
		assertEquals("Hello from Robert", messageService.getAllUserMessages("Robert").get(0).getValue());	
	}
	
}
